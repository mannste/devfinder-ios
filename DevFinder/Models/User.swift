//
//  User.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-12-08.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import Foundation

struct User{
    var name:String
    var userId:String
    var username:String
}
