//
//  Post.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-09-30.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit

class Post{
    
    var avatarImage: UIImage = UIImage()
    var postAuthor: String = ""
    var postContent: String = ""
    
    
}
