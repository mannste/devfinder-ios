//
//  Message.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-09-27.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

class Message {
    
    var messageBody: String = ""
    var messageSender: String = ""
    
}
