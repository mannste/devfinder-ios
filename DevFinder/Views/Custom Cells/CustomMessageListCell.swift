//
//  CustomMessageListCell.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-09-23.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit

class CustomMessageListCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var chatUsername: UILabel!
    
    var conversationId = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        conversationId = chatUsername.text!
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
