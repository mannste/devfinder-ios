//
//  CustomMessageCell.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-09-27.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit

class CustomMessageCell: UITableViewCell {
    
    @IBOutlet weak var messageContent: UILabel!
    @IBOutlet weak var messageSender: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
