//
//  CustomPostCell.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-09-30.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit

class CustomPostCell: UITableViewCell {

    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var postContent: UITextView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
