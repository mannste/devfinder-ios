//
//  TimeLineController.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-09-22.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import Parse

class TimeLineController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    @IBOutlet weak var postTableView: UITableView!
    
    
    var posts: [Post] = [Post]()
    
    let storage = Storage.storage();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Timeline"
        
        postTableView.delegate = self
        postTableView.dataSource = self
        
        postTableView.register(UINib(nibName: "CustomPostCell", bundle: nil), forCellReuseIdentifier: "PostCell")

        retrievePosts()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CustomPostCell = postTableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! CustomPostCell
        
        cell.postContent.text = posts[indexPath.row].postContent
        cell.usernameLabel.text = posts[indexPath.row].postAuthor
        cell.avatarImage.image = posts[indexPath.row].avatarImage
        
        
        cell.avatarImage.layer.cornerRadius = cell.avatarImage.layer.frame.width/2
       cell.avatarImage.clipsToBounds = true
        
        return cell
    }
    
    //retrieves all posts from Firebase
    func retrievePosts(){
        
         let postDB = Database.database().reference().child("feed/")
        
        postDB.observe(.value){ (snapshot) in
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                for child in snapshots{
                    if let snapshots = child.children.allObjects as? [DataSnapshot]{
                        for child in snapshots{
                            let snapshotValue = child.value as! Dictionary<String, NSObject>
                            
                            let postContent = snapshotValue["feedBody"]!
                            
                            let userSnapshot = child.childSnapshot(forPath: "user")
                            let userValue = userSnapshot.value as! Dictionary<String, NSObject>
                            
                            let postAuthor = userValue["username"]
                            let postAvatar = userValue["avatar"] as? String ?? ""
                            
                            let post = Post()
                            post.postContent = postContent as? String ?? ""
                            post.postAuthor = postAuthor as? String ?? ""
                            
                            let image = self.storage.reference(forURL: postAvatar)
                            image.getData(maxSize: 1 * 1024 * 1024) { data, error in
                                if let error = error {
                                    print("error occured when fetching user image \(error)")
                                }
                                else{
                                    let image = UIImage(data: data!)
                                    post.avatarImage = image!
                                }
                                
                            }
                            
                            self.posts.append(post)
                            
                            self.postTableView.reloadData()
                            
                        }
                    }
                }
            }
            
            
        }
        
    }
    
    func configureTableView() {
        postTableView.rowHeight = UITableViewAutomaticDimension
        postTableView.estimatedRowHeight = 120.0
    }
    

}
