//
//  PostController.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-11-18.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit
import Parse
import Firebase

class PostController: UIViewController {
    
    @IBOutlet weak var postContent: UITextField!
    @IBOutlet weak var postButton: UIButton!
    
    //gets the current parse user
    let user = PFUser.current()?.objectId
    var fbUserDict: [String:Any] = ["":""]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        postButton.layer.cornerRadius = 20
    }
    
    //handles when a user has sent a post to the DB
    @IBAction func postButton(_ sender: Any) {
        
        let feedDB = Database.database().reference().child("feed/\(user ?? "")").childByAutoId()
        let userDB = Database.database().reference().child("users/\(user ?? "")")
        
        //Closure checks that the user is valid within the Firebase structure, if exists the post will be set
        userDB.observe(.value, with: { (snapshot) in
            
            if(snapshot.exists()){
                self.fbUserDict = ["avatar": snapshot.childSnapshot(forPath: "avatar").value as? String ?? "", "email": snapshot.childSnapshot(forPath: "email").value as? String ?? "", "userId": snapshot.childSnapshot(forPath: "userId").value as? String ?? "", "username":snapshot.childSnapshot(forPath: "username").value as? String ?? "", "userStatus":snapshot.childSnapshot(forPath: "userStatus").value as? Int ?? 0]
                
                //dictionary to send to the feed table
                let feedDictionary: [String:Any] = ["feedBody": self.postContent.text ?? "", "feedId": feedDB.key ?? "", "user": self.fbUserDict]
                
                // inserts the feed into the feed table
                feedDB.setValue(feedDictionary){
                    (error, reference) in
                    
                    if(error != nil){
                        print(error!)
                    }
                    else {
                        print("Feed sent successfully")
                    }
                }
            }
        })
        
    }
    
}
