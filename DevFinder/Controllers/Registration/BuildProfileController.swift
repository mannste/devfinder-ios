//
//  BuildProfileController.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-12-10.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit
import Parse

class BuildProfileController: UIViewController {

    //UI Properties
    @IBOutlet weak var bioText: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    //Gets current users
    let currentUser = PFUser.current()?.objectId
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nextButton.layer.cornerRadius = 20

        
    }
    
    //sends data to parse segues to education page
    @IBAction func nextButton(_ sender: Any) {
        let bio = bioText.text
        
        if(bio == ""){
            bioText.layer.borderColor = UIColor.flatRed()?.cgColor
        }
        else{
            let userBio = PFObject(className: "profile")
            userBio["userId"] = currentUser
            userBio["aboutUser"] = bio
            userBio.saveInBackground {
                (success: Bool, error: Error?) in
                if (success) {
                    print("Object was saved")
                    self.performSegue(withIdentifier: "showEducation", sender: self)
                } else {
                    print(error)
                }
            }
        }
        
    }
    

}
