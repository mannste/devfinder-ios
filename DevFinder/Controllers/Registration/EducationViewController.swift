//
//  EducationViewController.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-12-10.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit
import Parse

class EducationViewController: UIViewController {

    //UI props
    @IBOutlet weak var schoolNameText: UITextField!
    @IBOutlet weak var schoolCountryText: UITextField!
    @IBOutlet weak var schoolCityText: UITextField!
    @IBOutlet weak var descriptionText: UITextField!
    @IBOutlet weak var programText: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    //gets current user
    let currentUser = PFUser.current()?.objectId
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nextButton.layer.cornerRadius = 20
        
    }
    
    //sends education info to Parse database then segues to Work Experience
    @IBAction func next(_ sender: Any) {
        let schoolName = schoolNameText.text
        let schoolCountry = schoolCountryText.text
        let schoolCity = schoolCityText.text
        let description = descriptionText.text
        let program = programText.text
        
        if(schoolName == ""){
            schoolNameText.layer.borderColor = UIColor.flatRed()?.cgColor
        }
        else if(schoolCountry == ""){
            schoolCountryText.layer.borderColor = UIColor.flatRed()?.cgColor
        }
        else if(schoolCity == ""){
            schoolCityText.layer.borderColor = UIColor.flatRed()?.cgColor
        }
        else if(description == ""){
            descriptionText.layer.borderColor = UIColor.flatRed()?.cgColor
        }
        else if(program == ""){
            programText.layer.borderColor = UIColor.flatRed()?.cgColor
        }
        else{
            let userEducation = PFObject(className: "education")
            userEducation["userId"] = currentUser
            userEducation["school"] = schoolName
            userEducation["schoolCountry"] = schoolCountry
            userEducation["schoolCity"] = schoolCity
            userEducation["description"] = description
            userEducation["program"] = program
            userEducation.saveInBackground {
                (success: Bool, error: Error?) in
                if (success) {
                    print("Object was saved")
                    self.performSegue(withIdentifier: "toWorkExperience", sender: self)
                } else {
                    print(error)
                }
            }
        }
    }
    

}
