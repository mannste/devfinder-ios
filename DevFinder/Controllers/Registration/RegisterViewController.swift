//
//  RegisterViewController.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-09-20.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit
import Parse
import Firebase
import FirebaseDatabase
import SVProgressHUD

class RegisterViewController: UIViewController {

    //UI Props
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerButton.layer.cornerRadius = 20

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func registerButton(_ sender: Any) {
        
        SVProgressHUD.show()
        
        //Variable instantiation
        let user = PFUser()
        let usersDB = Database.database().reference().child("users")
        
        user.username = usernameText.text
        user.email = emailText.text
        user.password = passwordText.text
        
        //attempt sign up
        user.signUpInBackground { (Bool, NSError) in
            if let error = NSError {
                print(error)
            } else {
                print("Registration Succeeded")
                
                let currentUser = PFUser.current()?.objectId
                
                //sign up into the Firebase service
                usersDB.child(currentUser!).setValue(["avatar": "NO AVATAR", "email": self.emailText.text!, "userId": currentUser!, "userStatus": 1, "username": self.usernameText.text!]) {
                    (error, reference) in
                    
                    if(error != nil){
                        print(error!)
                    } else {
                        print("user Saved Successfully")
                        SVProgressHUD.dismiss()
                    }
                    
                }
                
            }
        }
        
        
        
        
    }

}
