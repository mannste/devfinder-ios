//
//  WorkExperienceViewController.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-12-10.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit
import Parse

class WorkExperienceViewController: UIViewController {
    
    //UI Property initialization
    @IBOutlet weak var companyNameText: UITextField!
    @IBOutlet weak var jobTitleText: UITextField!
    @IBOutlet weak var countryText: UITextField!
    @IBOutlet weak var cityText: UITextField!
    @IBOutlet weak var descriptionText: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    //gets current user
    let currentUser = PFUser.current()?.objectId
    
    override func viewDidLoad() {
        super.viewDidLoad()

        registerButton.layer.cornerRadius = 20
       
    }
    
    //sends the user work experience to Parse then segues to
    @IBAction func register(_ sender: Any) {
        
        let companyName = companyNameText.text
        let jobTitle = jobTitleText.text
        let country = countryText.text
        let city = cityText.text
        let descritpion = descriptionText.text
        
        if(companyName == ""){
            companyNameText.layer.borderColor = UIColor.flatRed()?.cgColor
        }
        else if(jobTitle == ""){
            jobTitleText.layer.borderColor = UIColor.flatRed()?.cgColor
        }
        else if(country == ""){
            countryText.layer.borderColor = UIColor.flatRed()?.cgColor
        }
        else if(city == ""){
            cityText.layer.borderColor = UIColor.flatRed()?.cgColor
        }
        else if(descritpion == ""){
            descriptionText.layer.borderColor = UIColor.flatRed()?.cgColor
        }
        else{
            let userWorkExperience = PFObject(className: "education")
            userWorkExperience["userId"] = currentUser
            userWorkExperience["company"] = companyName
            userWorkExperience["jobTitle"] = jobTitle
            userWorkExperience["country"] = country
            userWorkExperience["city"] = city
            userWorkExperience["description"] = descritpion
            userWorkExperience["jobStatus"] = true
            userWorkExperience.saveInBackground {
                (success: Bool, error: Error?) in
                if (success) {
                    print("Object was saved")
                    self.performSegue(withIdentifier: "toTimeline", sender: self)
                } else {
                    print(error)
                }
            }
        }
        
    }
    
}
