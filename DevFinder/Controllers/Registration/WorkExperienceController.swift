//
//  WorkExperienceController.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-09-24.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit

class WorkExperienceController: UIViewController {

    @IBOutlet weak var jobTitle: UITextField!
    @IBOutlet weak var companyName: UITextField!
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var startDate: UITextField!
    @IBOutlet weak var endDate: UITextField!
    @IBOutlet weak var descriptionText: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startDatePicker(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePickerStart(sender: )), for: .valueChanged)
    }
    
    @IBAction func endDatePicker(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePickerEnd(sender: )), for: .valueChanged)
        
    }
    
    @objc func handleDatePickerStart(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy mm dd"
        startDate.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func handleDatePickerEnd(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy mm dd"
        endDate.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func nextButton(_ sender: Any) {
        
        
    }
    
}
