//
//  LoginViewController.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-09-20.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit
import Parse
import Firebase
import SVProgressHUD

class LoginViewController: UIViewController {
    
    //UI props
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.layer.cornerRadius = 20
        
        let tapRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(LoginViewController.dismissKeyboard))
        
        self.view.addGestureRecognizer(tapRecognizer)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //handles loginButton
    @IBAction func loginButton(_ sender: Any) {
        //variable instantiation
        let username = usernameText.text
        let password = passwordText.text
        
        SVProgressHUD.show()
        
        //attempt login to parse server
        do {
            try PFUser.logIn(withUsername: username!, password: password!)
            print("Login was successful!")
            SVProgressHUD.dismiss()
            self.performSegue(withIdentifier: "goToTimeline", sender: self)
        } catch {
            print("error logging in")
            SVProgressHUD.dismiss()
        }

    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    

}
