//
//  FindFriendsController.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-11-18.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit
import Parse
import CoreLocation
import MapKit

class FindFriendsController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    
    //UI Props
    @IBOutlet weak var userTable: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    
    //Selected row prop for click handling by cell
    var selectedRow:Int = -1
    
    //locationmanager instantiation
    let locationManager = CLLocationManager()
    
    //list of User objects
    var users: [User] = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //request location Tracking
        locationManager.requestWhenInUseAuthorization()
        
        if(CLLocationManager.locationServicesEnabled()){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        if let userLocation = locationManager.location?.coordinate {
            let viewRegion = MKCoordinateRegionMakeWithDistance(userLocation, 1000, 1000)
            mapView.setRegion(viewRegion, animated: true)
        }
        DispatchQueue.main.async {
            self.locationManager.stopUpdatingLocation()
        }
        
        // Gets all users within the Parse DB - stores into the list
        let userQuery = PFQuery(className: "_User")
        userQuery.findObjectsInBackground(){ (objects: [PFObject]?, error: Error?) in
            if let error = error {
                // Log details of the failure
                print(error.localizedDescription)
            } else if let objects = objects {
                // The find succeeded.
                print("Successfully retrieved \(objects.count) users.")
                // Do something with the found objects
                for object in objects {
                    var user: User = User(name: "", userId: "", username: "")
                    user.username = object.value(forKey: "username") as? String ?? ""
                    user.userId = object.value(forKey: "objectId") as? String ?? ""
                    user.name = object.value(forKey: "name") as? String ?? ""
                    
                    self.users.append(user)
                    self.placeMarker(user: user)
                }
                DispatchQueue.main.async {
                    self.userTable.reloadData()
                }
            }
        }
        
        userTable.dataSource = self
        userTable.delegate = self
        
    }
    
    //places markers for each user based on lat and long
    func placeMarker(user: User){
        
        let locQuery = PFQuery(className: "location")
        locQuery.whereKey("userId", equalTo: user.userId)
        locQuery.whereKey("status", equalTo: "public")
        locQuery.getFirstObjectInBackground { (object: PFObject?, error: Error?) in
            if let error = error {
                // The query failed
                print(error.localizedDescription)
            } else if let object = object {
                let lat = object.value(forKey: "lat") as? Double ?? 0
                let long = object.value(forKey: "long") as? Double ?? 0
                
                let loc = MKPointAnnotation()
                loc.title = user.username
                loc.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                self.mapView.addAnnotation(loc)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = userTable.dequeueReusableCell(withIdentifier: "userCell", for: indexPath)
        
        let index = indexPath.row
        let user = users[index]
        
        cell.textLabel?.text = "\(user.username)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showProfile", sender: nil)
    }
    
    // Sends the user information needed to query to UserProfile
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let userProfileController:UserProfileController = segue.destination as! UserProfileController
        selectedRow = userTable.indexPathForSelectedRow!.row
        userProfileController.masterView = self
        userProfileController.username = users[selectedRow].username
        userProfileController.userId = users[selectedRow].userId
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coord = locations.first?.coordinate {
            locationManager.stopUpdatingLocation()
            print(coord)
        }
    }
    
    
    
}
