//
//  UserProfileControllerViewController.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-12-09.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import Parse

class UserProfileController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //UI Props
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userContent: UITextView!
    @IBOutlet weak var postTable: UITableView!
    @IBOutlet weak var profileOption: UISegmentedControl!
    
    //vars sent from FindFriendsController
    var username = ""
    var userId = ""
    var masterView: FindFriendsController!
    
    let storage = Storage.storage();
    
    var posts: [Post] = [Post]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userName.text = username
        getBio()
        
        getUserAvatar()
        
        postTable.register(UINib(nibName: "CustomPostCell", bundle: nil), forCellReuseIdentifier: "PostCell")
        getPosts()
        
        avatarImage.layer.cornerRadius = avatarImage.layer.frame.width/2
        avatarImage.clipsToBounds = true
        
        postTable.delegate = self
        postTable.dataSource = self

    }
    
    //gets the users bio
    func getBio(){
        self.userContent.text = ""
        
        let query = PFQuery(className: "profile")
        query.whereKey("userId", equalTo: userId)
        query.getFirstObjectInBackground{
            (object: PFObject?, error: Error?)
            in
            if let error = error {
                print(error.localizedDescription)
            }
            else if let object = object{
                self.userContent.text = object.object(forKey: "aboutUser") as? String
            }
        }
        
    }
    
    //gets the users work experience
    func getWork(){
        self.userContent.text = ""
        
        let query = PFQuery(className: "workExperience")
        query.whereKey("userId", equalTo: userId)
        query.getFirstObjectInBackground{
            (object: PFObject?, error: Error?)
            in
            if let error = error {
                print(error.localizedDescription)
            }
            else if let object = object{
                let company = object.object(forKey: "company") as? String ?? ""
                let description = object.object(forKey: "description") as? String ?? ""
                let title = object.object(forKey: "jobTitle") as? String ?? ""
                let country = object.object(forKey: "country") as? String ?? ""
                let city = object.object(forKey: "city") as? String ?? ""
                
                self.userContent.text = """
                Company: \(company)
                Job Title: \(title)
                Description: \(description)
                Location: \(city) \(country)
                """
            }
        }
        
    }
    
    //gets the users education
    func getEducation(){
        
        self.userContent.text = ""
        
        let query = PFQuery(className: "education")
        query.whereKey("userId", equalTo: userId)
        query.getFirstObjectInBackground{
            (object: PFObject?, error: Error?)
            in
            if let error = error {
                print(error.localizedDescription)
            }
            else if let object = object{
                let school = object.object(forKey: "school") as? String ?? ""
                let program = object.object(forKey: "program") as? String ?? ""
                let country = object.object(forKey: "schoolCountry") as? String ?? ""
                let description = object.object(forKey: "description") as? String ?? ""
                
                self.userContent.text = """
                School: \(school)
                Program: \(program)
                Country: \(country)
                Description: \(description)
                """
            }
        }
        
    }
    
    //gets the tags associated with the user
    func getTags(){
        
        self.userContent.text = ""
        
        let query = PFQuery(className: "tags")
        query.whereKey("userId", equalTo: userId)
        query.getFirstObjectInBackground{
            (object: PFObject?, error: Error?)
            in
            if let error = error {
                print(error.localizedDescription)
            }
            else if let object = object{
                let tags = object.object(forKey: "tags") as? Array<Any>
                
                self.userContent.text = "Tags: "
                
                for tag in tags!{
                    self.userContent.text?.append("\(tag)" + ", ")
                }
            }
        }
    }
    
    //gets the posts the user has made
    func getPosts(){
        
        let postDB = Database.database().reference().child("feed/\(userId)")
        
        postDB.observe(.childAdded){ (snapshot) in
            
            let snapshotValue = snapshot.value as! Dictionary<String, NSObject>
            
            let postContent = snapshotValue["feedBody"]!
            
            let post = Post()
            post.postContent = postContent as? String ?? ""
            post.postAuthor = self.username
            
            self.posts.append(post)
            
            self.postTable.reloadData()
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CustomPostCell = postTable.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! CustomPostCell
        
        cell.postContent.text = posts[indexPath.row].postContent
        cell.usernameLabel.text = posts[indexPath.row].postAuthor
        cell.avatarImage.image = avatarImage.image
        
        cell.avatarImage.layer.cornerRadius = cell.avatarImage.layer.frame.width/2
        cell.avatarImage.clipsToBounds = true
        
        return cell
    }
    
    //gets the user avatar
    func getUserAvatar(){
        let userDB = Database.database().reference().child("users/\(userId)")
        
        userDB.observe(.value) { (snapshot) in
            
            let snapshotValue = snapshot.value as! Dictionary<String,NSObject>
            
            let avatar = snapshotValue["avatar"] as? String ?? ""
            let image = self.storage.reference(forURL: avatar)
            image.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    print("error occured when fetching user image \(error)")
                }
                else{
                    let image = UIImage(data: data!)
                    self.avatarImage.image = image
                    
                }
                
            }
        }
    }
    
    //handles the selected segment
    @IBAction func profileAction(_ sender: Any) {
        switch profileOption.selectedSegmentIndex {
        case 0:
            getBio()
        case 1:
            getWork()
        case 2:
            getEducation()
        case 3:
            getTags()
        default:
            getBio()
        }
    }
    

}
