//
//  ProfileController.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-09-24.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit
import Parse
import FirebaseStorage
import Firebase
import FirebaseDatabase

class ProfileController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //UI Props
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var linkCount: UILabel!
    @IBOutlet weak var postTable: UITableView!
    @IBOutlet weak var profileOption: UISegmentedControl!
    @IBOutlet weak var userContent: UITextView!
    @IBOutlet weak var editButton: UIButton!
    
    //FB Storage and user properties
    let storage = Storage.storage();
    let user = PFUser.current()?.objectId
    
    var userAvatar: UIImage = UIImage()
    
    //list of posts
    var posts: [Post] = [Post]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        editButton.layer.cornerRadius = 10
    
        username.text = PFUser.current()?.username
        getBio()
        
        getUserAvatar()
        
        postTable.register(UINib(nibName: "CustomPostCell", bundle: nil), forCellReuseIdentifier: "PostCell")
        getPosts()
        
        avatarImage.layer.cornerRadius = avatarImage.frame.size.width/2
        avatarImage.clipsToBounds = true
        
        postTable.delegate = self
        postTable.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //gets the user bio
    func getBio(){
        self.userContent.text = ""
        
        let query = PFQuery(className: "profile")
        query.whereKey("userId", equalTo: user ?? "")
        query.getFirstObjectInBackground{
            (object: PFObject?, error: Error?)
            in
            if let error = error {
                print(error.localizedDescription)
            }
            else if let object = object{
                self.userContent.text = object.object(forKey: "aboutUser") as? String
            }
        }
        
    }
    
    //gets the user work experience
    func getWork(){
        self.userContent.text = ""
        
        let query = PFQuery(className: "workExperience")
        query.whereKey("userId", equalTo: user ?? "")
        query.getFirstObjectInBackground{
            (object: PFObject?, error: Error?)
            in
            if let error = error {
                print(error.localizedDescription)
            }
            else if let object = object{
                let company = object.object(forKey: "company") as? String ?? ""
                let description = object.object(forKey: "description") as? String ?? ""
                let title = object.object(forKey: "jobTitle") as? String ?? ""
                let country = object.object(forKey: "country") as? String ?? ""
                let city = object.object(forKey: "city") as? String ?? ""
                
                self.userContent.text = """
                Company: \(company)
                Job Title: \(title)
                Description: \(description)
                Location: \(city) \(country)
                """
            }
        }
        
    }
    
    //gets the users education
    func getEducation(){
        
        self.userContent.text = ""
        
        let query = PFQuery(className: "education")
        query.whereKey("userId", equalTo: user ?? "")
        query.getFirstObjectInBackground{
            (object: PFObject?, error: Error?)
            in
            if let error = error {
                print(error.localizedDescription)
            }
            else if let object = object{
                let school = object.object(forKey: "school") as? String ?? ""
                let program = object.object(forKey: "program") as? String ?? ""
                let country = object.object(forKey: "schoolCountry") as? String ?? ""
                let description = object.object(forKey: "description") as? String ?? ""
                
                self.userContent.text = """
                School: \(school)
                Program: \(program)
                Country: \(country)
                Description: \(description)
                """
            }
        }
        
    }
    
    //gets the tags associated with the user
    func getTags(){
        
        self.userContent.text = ""
        
        let query = PFQuery(className: "tags")
        query.whereKey("userId", equalTo: user ?? "")
        query.getFirstObjectInBackground{
            (object: PFObject?, error: Error?)
            in
            if let error = error {
                print(error.localizedDescription)
            }
            else if let object = object{
                let tags = object.object(forKey: "tags") as? Array<Any>
                
                self.userContent.text = "Tags: "
                
                for tag in tags!{
                    self.userContent.text?.append("\(tag)" + ", ")
                }
            }
        }
    }
    
    //gets the posts the user has done
    func getPosts(){
        
        let postDB = Database.database().reference().child("feed/\(user ?? "")")
        
        postDB.observe(.childAdded){ (snapshot) in
            
            let snapshotValue = snapshot.value as! Dictionary<String, NSObject>
            
            let postContent = snapshotValue["feedBody"]!
            
            let post = Post()
            post.postContent = postContent as? String ?? ""
            post.postAuthor = PFUser.current()?.username ?? ""
            post.avatarImage = self.userAvatar
            
            self.posts.append(post)
            
            self.postTable.reloadData()
            
        }
        
    }
    
    //gets the user avatar
    func getUserAvatar(){
        let userDB = Database.database().reference().child("users/\(user ?? "")")
        
        userDB.observe(.value) { (snapshot) in
            
            let snapshotValue = snapshot.value as! Dictionary<String,NSObject>
            
            let avatar = snapshotValue["avatar"] as? String ?? ""
            let image = self.storage.reference(forURL: avatar)
            image.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    print("error occured when fetching user image \(error)")
                }
                else{
                    let image = UIImage(data: data!)
                    self.avatarImage.image = image
                }
            
            }
        }
    }
    
    //based on selected segment will get what is required
    @IBAction func profileAction(_ sender: Any) {
        
        switch profileOption.selectedSegmentIndex {
        case 0:
            getBio()
        case 1:
            getWork()
        case 2:
            getEducation()
        case 3:
            getTags()
        default:
            getBio()
        }
        
    }
    
    //allows the user content to be edited
    @IBAction func editContent(_ sender: Any) {
        
        if editButton.currentTitle == "Edit"{
            editButton.setTitle("Save", for: .normal)
            userContent.isEditable = true
        }
        else if editButton.currentTitle == "Save"{
            editButton.setTitle("Edit", for: .normal)
            userContent.isEditable = false
            
            switch profileOption.selectedSegmentIndex {
            case 0:
                setBio()
            default:
                return
            }
        }
        
    }
    
    //sets the bio once editing is complete
    func setBio(){
        let query = PFQuery(className: "profile")
        query.whereKey("userId", equalTo: user ?? "")
        query.getFirstObjectInBackground{
            (object: PFObject?, error: Error?)
            in
            if let error = error {
                print(error.localizedDescription)
            }
            else if let object = object{
                object["aboutUser"] = self.userContent.text
            }
            object?.saveInBackground()
        }
            
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CustomPostCell = postTable.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! CustomPostCell
        
        cell.postContent.text = posts[indexPath.row].postContent
        cell.usernameLabel.text = posts[indexPath.row].postAuthor
        cell.avatarImage.image = avatarImage.image
        
        return cell
    }
}
