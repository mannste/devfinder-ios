//
//  MessageController.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-09-22.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import Parse
import ChameleonFramework

class MessageController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    //UI props
    @IBOutlet weak var messageTableVIew: UITableView!
    @IBOutlet weak var messageEditText: UITextField!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendButton: UIButton!
    
    //prop init for required queries
    var conversationName = ""
    var conversationId = ""
    var recieverId = ""
    
    //init masterview
    var masterView: MessageListController!
    
    //init array of messages
    var messageArray : [Message] = [Message]()
    
    //get sys time
    var time = Date()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = conversationName
        
        sendButton.layer.cornerRadius = 10
        
        messageEditText.layer.shadowColor = UIColor.black.cgColor
        messageEditText.layer.shadowRadius = 3.0
        messageEditText.layer.shadowOpacity = 0.5
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tableViewTapped))
        messageTableVIew.addGestureRecognizer(tapGesture)
        
        messageTableVIew.register(UINib(nibName: "MessageCellBubble", bundle: nil), forCellReuseIdentifier: "customMessageCellBubble")
        retrieveMessages()
        
        messageTableVIew.separatorStyle = .none
        
        messageTableVIew.delegate = self
        messageTableVIew.dataSource = self
        
        messageEditText.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //retrieves messages from the conversation
    func retrieveMessages(){
        
        let messageDB = Database.database().reference().child("messages/\(conversationId)")
        
        messageDB.observe(.childAdded) { (snapshot) in
            
            let snapshotValue = snapshot.value as! Dictionary<String, NSObject>
            
            let messageContent = snapshotValue["body"]!
            let messageSender = snapshotValue["senderId"]!
            
            let message = Message()
            message.messageBody = messageContent as! String
            message.messageSender = messageSender as! String
            
            self.messageArray.append(message)
            
            self.messageTableVIew.reloadData()
            
        }
        
    }
    
    //handles sending chat message to the FB database
    @IBAction func sendPressed(_ sender: Any) {
        
        let user = PFUser.current()?.objectId!
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.amSymbol = "am"
        dateFormatter.pmSymbol = "pm"
        let myTime = dateFormatter.string(from: time)
        
        messageEditText.endEditing(true)
        
        messageEditText.isEnabled = false
        sendButton.isEnabled = false
        
        let messagesDB = Database.database().reference().child("messages/")
        
        let messageDictionary = ["body": messageEditText.text ?? "", "recieverId": recieverId, "senderId": user ?? "", "timeSent": myTime] as [String : Any]
        
        messagesDB.child("\(conversationId)").childByAutoId().setValue(messageDictionary){
            (error, reference) in
            
            if(error != nil){
                print(error!)
            }
            else {
                print("Message sent successfully")
                
                self.messageEditText.isEnabled = true
                self.sendButton.isEnabled = true
                self.messageEditText.text = ""
            }
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "customMessageCellBubble", for: indexPath) as! CustomMessageCell
        
        cell.messageContent.text = messageArray[indexPath.row].messageBody
        cell.messageContent.layer.masksToBounds = true
        cell.messageContent.layer.cornerRadius = 20
        if(messageArray[indexPath.row].messageSender == recieverId){
            cell.messageSender.text = conversationName
        }
        else {
            cell.messageSender.text = PFUser.current()?.username
        }
        
        
        if(cell.messageSender.text == conversationName){
            //Messages user has sent
            cell.messageContent.backgroundColor = UIColor.gray
        }
        else{
            //Messages other user has sent
            cell.messageContent.backgroundColor = UIColor.flatPowderBlue()
        }
        
        return cell
    }
    
    @objc func tableViewTapped(){
        
        messageEditText.endEditing(true)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.heightConstraint.constant = 308
            self.view.layoutIfNeeded()
        })
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.heightConstraint.constant = 50
            self.view.layoutIfNeeded()
        })
        
    }
    
    
    
    

}
