//
//  MessageListController.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-09-22.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import Parse

class MessageListController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    //UI Props
    @IBOutlet weak var chatTableView: UITableView!
    
    //get selected row
    var selectedRow:Int = -1
    
    //array of chat objects
    var chatArray : [Chat] = [Chat]()
    
    //Parse user and Firebase Storage
    let user = PFUser.current()?.objectId!
    let storage = Storage.storage()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        chatTableView.delegate = self
        chatTableView.dataSource = self
        
        chatTableView.register(UINib(nibName: "MessageListCell", bundle: nil), forCellReuseIdentifier: "customMessageListCell")
        retrieveChats()
        
        chatTableView.separatorStyle = .none
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return chatArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "customMessageListCell", for: indexPath) as! CustomMessageListCell
        
        let cellImage = storage.reference(forURL: chatArray[indexPath.row].avatar)
        cellImage.getData(maxSize: 1 * 1024 * 1024) { data, error in
            if let error = error {
                print("error occured when fetching user image: \(error)")
            }
            else{
                let image = UIImage(data: data!)
                cell.avatarImageView.image = image
            }
            
        }
            
        cell.chatUsername.text = chatArray[indexPath.row].conversation
        cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.layer.frame.width/2
        cell.avatarImageView.clipsToBounds = true
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "conversation", sender: nil)
    }
    
    //send data required for messaging to messageController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let messageController:MessageController = segue.destination as! MessageController
        selectedRow = chatTableView.indexPathForSelectedRow!.row
        messageController.masterView = self
        messageController.conversationName = chatArray[selectedRow].conversation
        messageController.recieverId = chatArray[selectedRow].id
        
        if(user! > chatArray[selectedRow].id){
            messageController.conversationId = "\(user ?? "")\(chatArray[selectedRow].id)"
        }
        else if(user! < chatArray[selectedRow].id){
            messageController.conversationId = "\(chatArray[selectedRow].id)\(user ?? "")"
        }
        else{
            messageController.conversationId = "\(user ?? "")\(user ?? "")"
        }
    }
    
    //retrieve all conversations from Firebase
    func retrieveChats(){
        
        let chatDB = Database.database().reference().child("users")
        
        chatDB.observe(.childAdded) { (snapshot) in
            
            let snapshotValue = snapshot.value as! Dictionary<String,NSObject>
            
            let username = snapshotValue["username"] as? String ?? ""
            let id = snapshotValue["userId"] as? String ?? ""
            let avatar = snapshotValue["avatar"] as? String ?? ""
            
            let chat = Chat()
            chat.conversation = username
            chat.id = id
            chat.avatar = avatar
            
            self.chatArray.append(chat)
            
            self.chatTableView.reloadData()
            
        }
        
    }
    
}
