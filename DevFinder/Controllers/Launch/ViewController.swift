//
//  ViewController.swift
//  DevFinder
//
//  Created by Steven Mann on 2018-09-18.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit
import Parse
import FirebaseDatabase

class ViewController: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.layer.cornerRadius = 20
        registerButton.layer.cornerRadius = 20
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

